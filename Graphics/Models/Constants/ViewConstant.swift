//
//  ViewConstant.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import Foundation

struct PaddingSize {
  static let extraSmall: Double = 4.0
  static let small: Double = 8.0
  static let medium: Double = 12.0
  static let defaultVal: Double = 16.0
  static let large: Double = 24.0
  static let extraLarge: Double = 34.0
}
