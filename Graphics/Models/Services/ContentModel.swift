//
//  ContentModel.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import Foundation

struct ContentModel: Codable {
  var noOfComments: Int
  var sentiment: String
  var sentimentScore: Double
  var ticker: String
  
  enum CodingKeys: String, CodingKey {
    case noOfComments = "no_of_comments"
    case sentimentScore = "sentiment_score"
    case sentiment, ticker
  }
}
