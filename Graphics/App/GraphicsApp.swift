//
//  GraphicsApp.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import SwiftUI

@main
struct GraphicsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
