//
//  ContentViewModel.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import SwiftUI

class ContentViewModel: ObservableObject {
  @Published var isLoading: Bool = false
  @Published var isError: Bool = false
  @Published var response: [ContentModel] = []
  
  func fetch() {
    isLoading = true
    ContentHandler.sharedInstance.fetch { completion, data in
      if completion {
        self.response = data!
        self.isLoading = false
      } else {
        self.isError = true
      }
    }
  }
}
