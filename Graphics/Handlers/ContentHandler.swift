//
//  ContentHandler.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import Foundation
import Alamofire

class ContentHandler: BaseHandler {
  static let sharedInstance = ContentHandler()
  
  func fetch(completionHandler: @escaping (Bool, [ContentModel]?) -> ()) {
    AF.request(
      URLConstant.baseURL,
      method: .get,
      headers: baseHeader
    ).response { res in
      switch res.result {
      case .success(let data):
        do {
          if res.response?.statusCode == 200 {
            let parsedData = try JSONDecoder().decode([ContentModel].self, from: data!)
            completionHandler(true, parsedData)
          } else {
            completionHandler(false, nil)
          }
        } catch {
          completionHandler(false, nil)
        }
      case .failure(let err):
        debugPrint(err)
        completionHandler(false, nil)
      }
    }
  }
}
