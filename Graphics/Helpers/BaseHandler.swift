//
//  BaseHandler.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import Foundation
import Alamofire

class BaseHandler {
  let baseHeader: HTTPHeaders = [
    .contentType("application/json")
  ]
}
