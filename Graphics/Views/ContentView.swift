//
//  ContentView.swift
//  Graphics
//
//  Created by Ardyan Wahyu on 13/10/21.
//

import SwiftUI

struct ContentView: View {
  
  
  @ObservedObject private var viewModel = ContentViewModel()
  
  init() {
    viewModel.fetch()
  }
  
  var body: some View {
    NavigationView {
      GeometryReader { g in
        if viewModel.isLoading {
          VStack(spacing: 0) {
            ForEach(0..<7) {_ in
              ItemView(
                sentiment: "",
                title: "lorem ipsum",
                commentsCount: 0,
                score: 0
              )
              .redacted(reason: .placeholder)
              .padding(.top, PaddingSize.small)
              .padding(.bottom, PaddingSize.extraSmall)
            }
          }
        } else {
          ScrollView(.vertical, showsIndicators: false) {
            VStack(spacing: 0) {
              ForEach(0..<viewModel.response.count) { i in
                ItemView(
                  sentiment: viewModel.response[i].sentiment,
                  title: viewModel.response[i].ticker,
                  commentsCount: viewModel.response[i].noOfComments,
                  score: viewModel.response[i].sentimentScore
                )
                .padding(.top, PaddingSize.small)
                .padding(.bottom, PaddingSize.extraSmall)
              }
              
              HStack {}
              .frame(width: .infinity, height: 34)
            }
          }
        }
      }
      .edgesIgnoringSafeArea(.bottom)
      .navigationTitle("Graphics")
      .navigationBarItems(
        leading: Image("Icon")
          .resizable()
          .scaledToFit()
          .frame(width: 24)
          .padding(.trailing, PaddingSize.small)
      )
      .alert(isPresented: $viewModel.isError) {
        Alert(
          title: Text("Tidak Dapat Terhubung"),
          message: Text("Segera periksa koneksi internet anda kemudian segarkan"),
          dismissButton: .default("Segarkan") {
            viewModel.fetch()
          }
        )
      }
    }
  }
}

struct ItemView: View {
  let sentiment: String
  let title: String
  let commentsCount: Int
  let score: Double
  
  var body: some View {
    HStack(spacing: 0) {
      Image(sentiment == "Bearish" ? "Loss" : "Growth")
        .resizable()
        .scaledToFit()
        .frame(width: 50)
        .padding(.trailing, PaddingSize.medium)
        .padding(.vertical, PaddingSize.medium)
      
      VStack(alignment: .leading, spacing: 0) {
        Text(title)
          .bold()
          .padding(.bottom, PaddingSize.extraSmall)
        
        Text(String(commentsCount))
          .font(.subheadline)
      }
      
      Spacer()
      
      Image(score.sign == .minus ? "Sad" : "Smile")
        .resizable()
        .scaledToFit()
        .frame(width: 24)
    }
    .padding(.horizontal)
    .background(
      Color("BGItems")
    )
    .cornerRadius(8)
    .shadow(
      color: Color.black.opacity(0.15),
      radius: 8,
      x: 0,
      y: 0
    )
    .padding(.horizontal)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
